#ifndef WS281X_PATTERN_CYLON_H
#define WS281X_PATTERN_CYLON_H

#include "ws2811_pattern.h"

namespace nanite {
  class ws2811_pattern_cylon : public ws2811_pattern {
      public:
        ws2811_pattern_cylon();
        ~ws2811_pattern_cylon();

        void next_frame();

      protected:
        rx_int position;    //!< where the light is now.
        rx_int hue;         //!< Current hue value, defaul 0, aka red.
        rx_int saturation;  //!< Current saturation default 100
        rx_int brightness;  //!< Brightnes 0-100%, default 100
        rx_int spread;      //!< How far the cylon is spread out default 7
        rx_int inc;         //!< Current increment for the position, default is 1.

        int * brightness_array;  //Array with all of the brightness values.  It fades by the spread.  Position sets the value to 100

  };
}

#endif // WS2811_PATTERN_H_
