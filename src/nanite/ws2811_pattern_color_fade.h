#ifndef WS281X_PATTERN_COLOR_FADE_H
#define WS281X_PATTERN_COLOR_FADE_H

#include "ws2811_pattern.h"

namespace nanite {
  class ws2811_pattern_color_fade : public ws2811_pattern {
      public:
        ws2811_pattern_color_fade();
        ~ws2811_pattern_color_fade();

        void next_frame();

      protected:
        rx_int brightness;
        rx_int max_brightness;
        rx_int inc;

  };
}

#endif // WS2811_PATTERN_H_
