#ifndef WS281X_PATTERN_H
#define WS281X_PATTERN_H

#include "nanite/rx/base/nanite.h"

namespace nanite {
  class ws2811_pattern : public rx_nanite {
      public:
        ws2811_pattern(const std::string &name="ws2811_pattern");
        ~ws2811_pattern();

        void delete_buffer();
        void set_num_leds(int n);

        void hsv_to_rgb(int hue, int sat, int val, int colors[3]);

      protected:
        rx_int num_leds;    //!< Number of leds to produce a frame.
        rx_bool next;       //!< Produces the next frame in the sequece
        rx_string frame;    //!< Frame produced after calling next.

        int * buffer;       //!< buffer for storing integers.  Convert to to char* and write into a string for the frame.
  };
}

#endif // WS2811_PATTERN_H_
