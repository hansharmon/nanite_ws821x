#include "ws2811_pattern_flash.h"

namespace nanite {
    ws2811_pattern_flash::ws2811_pattern_flash() : ws2811_pattern("ws2811_pattern_flash") {
        hue_bottom = 0;
        add_int("hue_bottom", hue_bottom );

        hue_top = 359;
        add_int("hue_top", hue_top );

        saturation_bottom = 99;
        add_int("saturation_bottom", saturation_bottom );

        saturation_top = 100;
        add_int("saturation_top", saturation_top );

        value_bottom = 99;
        add_int("value_bottom", value_bottom );

        value_top = 100;
        add_int("value_top", value_top );

        n = 1;
        add_int("n", n);

        next([=](bool b) { next_frame(); });
    }
    ws2811_pattern_flash::~ws2811_pattern_flash() {}

    void ws2811_pattern_flash::next_frame() {
        if (buffer == NULL) return;
        if (num_leds <= 0) return;

        // Black it out.
        for (int i = 0;i < num_leds; i++ ) {
              buffer[i] = 0;
        }

        // Pop of random color.
        for (int i = 0; i < n; i++ ) {
            int index = rand() % num_leds.get_value();
            int hue = hue_bottom        + (rand() % (hue_top        - hue_bottom ));
            int sat = saturation_bottom + (rand() % (saturation_top - saturation_bottom ));
            int val = value_bottom      + (rand() % (value_top      - value_bottom ));
            int color[3];
            hsv_to_rgb(hue,sat,val,color);
            buffer[index] = (color[0] << 16) | (color[1] << 8) | color[2];
        }
        frame = std::string((char*) buffer, num_leds*4);
    }
}

NANITE_GEN_PROXY(ws2811_pattern_flash,nanite::ws2811_pattern_flash)
