#include "ws2811_pattern_rainbow.h"

namespace nanite {
    ws2811_pattern_rainbow::ws2811_pattern_rainbow() : ws2811_pattern("ws2811_pattern_rainbow") {
        hue = 0;
        add_int("hue", hue);

        min_hue = 0;
        add_int("min_hue", min_hue);

        max_hue = 359;
        add_int("max_hue", max_hue);

        brightness = 100;
        add_int("brightness", brightness);

        inc = 1;
        add_int("inc",inc);

        spread = 360;
        add_int("spread",spread);

        next([=](bool b) { next_frame(); });
    }
    ws2811_pattern_rainbow::~ws2811_pattern_rainbow() {}

    void ws2811_pattern_rainbow::next_frame() {
        if (buffer == NULL) return;
        if (num_leds <= 0) return;
        hue += inc;
        if (hue > max_hue) {
            hue = min_hue+1;
        }
        if (hue < min_hue) {
            hue = max_hue-1;
        }
        int color[3];
        for (int i = 0;i < num_leds; i++ ) {
            hsv_to_rgb(hue+(i*spread)/num_leds,100,brightness,color);
            buffer[i] = (color[0] << 16) | (color[1] << 8) | color[2];
        }

        frame = std::string((char*) buffer, num_leds*4);
    }
}

NANITE_GEN_PROXY(ws2811_pattern_rainbow,nanite::ws2811_pattern_rainbow)
