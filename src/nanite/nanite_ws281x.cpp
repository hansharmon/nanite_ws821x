#include "nanite_ws281x.h"


#include <unistd.h>
#include <stdio.h>


namespace nanite {
  nanite_ws281x::nanite_ws281x() : rx_nanite("ws281x") {
    // Brightness is the only that will not be set by the code.
    ws281x_settings.channel[0].brightness = 255;

    // Make sure this is an unused channel.
    ws281x_settings.channel[1].gpionum = 0;
    ws281x_settings.channel[1].count = 0;
    ws281x_settings.channel[1].invert = 0;
    ws281x_settings.channel[1].brightness = 0;

    is_initialized = false;
    add_bool("is_initialized", is_initialized);

    initialize = false;
    initialize([=](bool b) { init(b); });
    add_bool("initialize",initialize);

    num_leds = 50;
    num_leds([=](int n){ws281x_settings.channel[0].count = n;});
    add_int("num_leds", num_leds);

    frequency = 800000;
    frequency([=](int f){ ws281x_settings.freq = f;});
    add_int("frequency",frequency);

    dma = 10;
    dma([=](int d) { ws281x_settings.dmanum=10; });
    add_int("dma",dma);

    gpio = 18;
    gpio([=](int g) { ws281x_settings.channel[0].gpionum = g; });
    add_int("gpio", gpio);

    strip_type = WS2811_STRIP_RGB;
    strip_type([=](int t) {ws281x_settings.channel[0].strip_type=t;});
    add_int("strip_type",strip_type);

    render = false;
    render([=](bool b) { if (b) { ws2811_render(&ws281x_settings); }});
    add_bool("render",render);

    color = 0x000000;
    color([=](int c) { set_color(c); });
    add_int("color", color);

    frame = "";
    frame([=](const std::string &str) { set_frame(str); });
    add_string("frame",frame);

  }

  nanite_ws281x::~nanite_ws281x() {
      color = 0;
      fini();
  }

  void nanite_ws281x::fini() {
    if (is_initialized) {
        ws2811_fini(&ws281x_settings);
        is_initialized = false;
    }
  }

  void nanite_ws281x::init(bool b) {
    if (!b) return;
    fini();
    ws2811_init(&ws281x_settings);

    for (int i=0;i<num_leds;i++) {
        rx_int * led = new rx_int();
        (*led)([=](int v) { ws281x_settings.channel[0].leds[i] = v; });
        add_int("led_" + to_string(i), led);
    }
    is_initialized = true;
  }

  void nanite_ws281x::set_color(int c) {
      if (!is_initialized) return;
      for (int i=0;i<num_leds;i++) {
        (*this)("led_" + to_string(i)) = c;
      }
      render = true;
  }

  void nanite_ws281x::set_frame(const std::string &frame) {
    if (frame.empty() ) return;

    int max_leds = frame.length() / 4;
    int * leds = (int*) (frame.c_str());
    if (max_leds > num_leds) max_leds = num_leds;
    for (int i = 0; i < max_leds; i++ ) {
      ws281x_settings.channel[0].leds[i] = leds[i];
    }
    render = true;
  }

};

NANITE_GEN_PROXY(ws281x,nanite::nanite_ws281x)
