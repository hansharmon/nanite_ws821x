#include "ws2811_pattern_color_fade.h"

namespace nanite {
    ws2811_pattern_color_fade::ws2811_pattern_color_fade() : ws2811_pattern("ws2811_pattern_color_fade") {
        brightness = 0;
        add_int("brightness", brightness);

        max_brightness = 0xFF;
        add_int("max_brightness", max_brightness);

        inc = 1;
        add_int("inc",inc);

        next([=](bool b) { next_frame(); });
    }
    ws2811_pattern_color_fade::~ws2811_pattern_color_fade() {}

    void ws2811_pattern_color_fade::next_frame() {
        if (buffer == NULL) return;
        if (num_leds <= 0) return;
        brightness+=inc;
        if (brightness > max_brightness) {
            brightness = (max_brightness-1);
            inc = -inc;
        }
        if (brightness < 0) {
            brightness = 0;
            inc = -inc;
        }
        int mb = max_brightness.get_value();
        int b = brightness.get_value();
        for (int i = 0;i < num_leds; i++ ) {
            buffer[i] = (b << ((i % 3) * 8)) | ((mb - b) << (((i+1) % 3) * 8));
        }

        frame = std::string((char*) buffer, num_leds*4);
    }
}

NANITE_GEN_PROXY(ws2811_pattern_color_fade,nanite::ws2811_pattern_color_fade)
