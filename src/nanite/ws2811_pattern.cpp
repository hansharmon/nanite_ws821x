
#include "ws2811_pattern.h"

namespace nanite {
    ws2811_pattern::ws2811_pattern(const std::string &name) : rx_nanite(name) {

        num_leds = 0;
        num_leds([=](int n) { set_num_leds(n); });
        add_int("num_leds", num_leds);

        next = false;
        add_bool("next",next);

        frame = "";
        add_string("frame", frame);

    }
    ws2811_pattern::~ws2811_pattern() {
        delete_buffer();
    }

    void ws2811_pattern::delete_buffer() {
        if (buffer != NULL) {
            delete [] buffer;
            buffer = NULL;
        }
    }

    void ws2811_pattern::set_num_leds(int n) {
        if (n <=  0) return;

        delete_buffer();
        buffer = new int[n];
    }

    void ws2811_pattern::hsv_to_rgb(int hue, int sat, int val, int colors[3]) {
        // hue: 0-359, sat: 0-100, val (lightness): 0-100
        // Allow for negative hues so that ranges below 0 can be mapped more easily from a random function.
        hue = hue % 360;
        if (hue < 0) hue += 360;
        int r, g, b, base;

        if (sat == 0) { // Achromatic color (gray).
            colors[0]=val;
            colors[1]=val;
            colors[2]=val;
        } else  {
            base = ((100 - sat) * val) / 100;
            switch(hue/60) {
                case 0:
                    r = val;
                    g = (((val-base)*hue)/60)+base;
                    b = base;
                    break;
                case 1:
                    r = (((val-base)*(60-(hue%60)))/60)+base;
                    g = val;
                    b = base;
                    break;
                case 2:
                    r = base;
                    g = val;
                    b = (((val-base)*(hue%60))/60)+base;
                    break;
                case 3:
                    r = base;
                    g = (((val-base)*(60-(hue%60)))/60)+base;
                    b = val;
                    break;
                case 4:
                    r = (((val-base)*(hue%60))/60)+base;
                    g = base;
                    b = val;
                    break;
                case 5:
                    r = val;
                    g = base;
                    b = (((val-base)*(60-(hue%60)))/60)+base;
                    break;
            }
            colors[0]=r;
            colors[1]=g;
            colors[2]=b;
        }
    }
}

NANITE_GEN_PROXY(ws2811_pattern,nanite::ws2811_pattern)
