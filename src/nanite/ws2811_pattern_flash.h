#ifndef WS281X_PATTERN_FLASH_H
#define WS281X_PATTERN_FLASH_H

#include "ws2811_pattern.h"

namespace nanite {
  class ws2811_pattern_flash : public ws2811_pattern {
      public:
        ws2811_pattern_flash();
        ~ws2811_pattern_flash();

        void next_frame();

      protected:
        rx_int hue_bottom;         //!< Bottom of a range of hues to flash
        rx_int hue_top;            //!< Top of range of hues to flash

        rx_int saturation_bottom;  //!< Bottom of a range of saturations to flash
        rx_int saturation_top;     //!< Top of range of saturations to flash

        rx_int value_bottom;       //!< Bottom of a range of values to flash
        rx_int value_top;          //!< Top of range of values to flash

        rx_int n;                  //!< Number of lights to pick for flashing different colors
  };
}

#endif // WS2811_PATTERN_H_
