#ifndef NANITE_WS281X_H
#define NANITE_WS281X_H

#include "nanite/rx/base/nanite.h"
#include <ws2811/ws2811.h>


namespace nanite {
  class nanite_ws281x : public rx_nanite {
      public:
        nanite_ws281x();
        ~nanite_ws281x();

        void fini();

        void init(bool b);

        void set_color(int color);

        void set_frame(const std::string &frame);

      protected:
        ws2811_t ws281x_settings;    //!< Main setup for the ws281x string controller.

        rx_bool is_initialized;     //!< Flags if the code has been initialized or not.     Defaults to false.
        rx_bool initialize; //!< Initilizes ws2811 library based on the current settings.   Defaults to false.

        rx_int num_leds;    //!< Number of leds, when set creates the string object.        Defualts to 50
        rx_int frequency;   //!< Frequency to communicate on.                               Defaults to 800000
        rx_int dma;         //!< DMA bank to use for byte transfer.                         Defaults to 10
        rx_int gpio;        //!< GPIO to use for RPM.                                       Defaults to 18

        //TODO:  Replace this with a string map to be interpertted to the correct enum.
        rx_int strip_type;  //!< Type of strip.                                             Defaults to WS2811_STRIP_RGB;

        rx_bool render;     //!< renders the LED values.


        rx_int color;       //!< Sets all of the leds to the same color, useful for testing.  Default to 0.

        rx_string frame;    //!< Sets a single frame of the LEDs.
  };

};


#endif // ANITE_WS281X_H
