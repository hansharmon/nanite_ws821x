#ifndef WS281X_PATTERN_RAINBOW_H
#define WS281X_PATTERN_RAINBOW_H

#include "ws2811_pattern.h"

namespace nanite {
  class ws2811_pattern_rainbow : public ws2811_pattern {
      public:
        ws2811_pattern_rainbow();
        ~ws2811_pattern_rainbow();

        void next_frame();

      protected:
        rx_int hue;         //!< Current hue value.
        rx_int min_hue;     //!< Minimum hue value 0-360
        rx_int max_hue;     //!< Maximum hue value 0-360
        rx_int brightness;  //!< Brightnes 0-100%
        rx_int inc;         //!< Increment between hue changes
        rx_int spread;      //!< How far the rainbow is spread out
  };
}

#endif // WS2811_PATTERN_H_
