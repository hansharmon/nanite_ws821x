#include "ws2811_pattern_cylon.h"

namespace nanite {
    ws2811_pattern_cylon::ws2811_pattern_cylon() : ws2811_pattern("ws2811_pattern_cylon") {
        hue = 0;
        add_int("hue", hue);

        saturation = 100;
        add_int("saturation", saturation );

        brightness = 100;
        add_int("brightness", brightness);

        position = 1;
        add_int("position",inc);

        spread = 5;
        add_int("spread",spread);

        inc = 1;
        add_int("inc",inc);

        next([=](bool b) { next_frame(); });

        brightness_array = NULL;
    }
    ws2811_pattern_cylon::~ws2811_pattern_cylon() {
        if ( brightness_array != NULL) {
            delete []brightness_array;
            brightness_array = NULL;
        }
    }

    void ws2811_pattern_cylon::next_frame() {
        if (buffer == NULL) return;
        if (num_leds <= 0) return;
        if (brightness_array == NULL) {
            brightness_array = new int[num_leds];
            for (int i=0;i<num_leds;i++) {
                brightness_array[i] = 0;
            }
        }

        position+=inc;
        if (position < 0) {
            position = 0;
            inc = -inc;
        }
        if (position > num_leds) {
            position = num_leds-1;
            inc = -inc;
        }
        int color[3];
        brightness_array[position] = 100;
        for (int i=0;i<num_leds;i++) {
            hsv_to_rgb(hue,saturation,brightness_array[i],color);
            buffer[i] = (color[0] << 16) | (color[1] << 8) | color[2];
            brightness_array[i] -= 100/spread;
            if (brightness_array[i] < 0) {
                brightness_array[i] = 0;
            }
        }

        frame = std::string((char*) buffer, num_leds*4);
    }
}

NANITE_GEN_PROXY(ws2811_pattern_cylon,nanite::ws2811_pattern_cylon)
