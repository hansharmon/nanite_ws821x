#include <ws2811/ws2811.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

  ws2811_t ledstring =
  {
    .freq = 800000,
    .dmanum = 10,
    .channel =
    {
        [0] =
        {
            .gpionum = 18,
            .count = 50,
            .invert = 0,
            .brightness = 255,
            .strip_type = WS2811_STRIP_RGB,
        },
        [1] =
        {
            .gpionum = 0,
            .count = 0,
            .invert = 0,
            .brightness = 0,
        },
    },
  };


//-CONVERT HSV VALUE TO RGB
void HSVtoRGB(int hue, int sat, int val, int colors[3]) {
	// hue: 0-359, sat: 0-255, val (lightness): 0-255
	int r, g, b, base;

	if (sat == 0) { // Achromatic color (gray).
		colors[0]=val;
		colors[1]=val;
		colors[2]=val;
	} else  {
		base = ((255 - sat) * val)>>8;
		switch(hue/60) {
			case 0:
				r = val;
				g = (((val-base)*hue)/60)+base;
				b = base;
				break;
			case 1:
				r = (((val-base)*(60-(hue%60)))/60)+base;
				g = val;
				b = base;
				break;
			case 2:
				r = base;
				g = val;
				b = (((val-base)*(hue%60))/60)+base;
				break;
			case 3:
				r = base;
				g = (((val-base)*(60-(hue%60)))/60)+base;
				b = val;
				break;
			case 4:
				r = (((val-base)*(hue%60))/60)+base;
				g = base;
				b = val;
				break;
			case 5:
				r = val;
				g = base;
				b = (((val-base)*(60-(hue%60)))/60)+base;
				break;
		}
		colors[0]=r;
		colors[1]=g;
		colors[2]=b;
	}
}



int main(int argc, char * argv[]) {
  ws2811_init(&ledstring);


  int brightness = 0;
  int max_brightness = 0xFF;
  int inc = 1;
  int offset = 0;
  int color[3];
  while(1) {
    brightness+=inc;
    if (brightness > max_brightness) {
      brightness = max_brightness;
      inc = -inc;
    }
    if (brightness < 0) {
      brightness = 0;
      inc = -inc;
//      offset++;
    }
    //printf("Brighness = %d\n", brightness );
    //brightness = 0;
    for (int i = 0;i < 50; i++ ) {
       //ledstring.channel[0].leds[i] = (brightness << 16) | (brightness << 8) | brightness;
       ledstring.channel[0].leds[i] = brightness << (((i+offset) % 3) * 8) | (max_brightness - brightness) << (((i+offset+1) % 3) * 8);
       if ( i == 0) {
         //printf("%04X\n",ledstring.channel[0].leds[i]);
       }
    }
    int index = rand() % 50;
    int hue = rand() % 359;
    HSVtoRGB(hue,255,255,color);
    //ledstring.channel[0].leds[index] = (color[0] << 16) | (color[1] << 8) | color[2];
    usleep(100000/30);
    ws2811_render(&ledstring);
  }

  return 0;
}
