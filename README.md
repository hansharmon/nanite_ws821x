# README #

This test code for rpi_ws281x, then builds on that library and nanite to create a nanite plug-in/block to control ws281x strips via nanite.

## Nanite WS281X ##

This repository is a control block to control WS281X device as part of the nanite library.
This also include some simple test code for useing the rpi_ws281x library.

* Test/Example Code for rpi_ws2811
* Baseline WS2821X Nanite
* Control individual light
* Control whole strand
* Control by frame of values, i.e. patterns/effects.

Currently this has been tested on a Raspberry Pi Zero W running Raspbian Lite.

### Test Code ###

Simple test example to code to get started with the ws2811 and the rpi-ws281x library.

### Contribution guidelines ###

Feedback is good, if you have project or would like to add more patterns feel free.
As an example look at the color_fade pattern.  This pluses the leds out a little bit.

### Future Work ###
Planned work includes:

* Adding a web front end demonstraction likely in REACT.
* Adding more patterns/effects.
* Adding positional information to the leds to help with patterns.
* Adding a way to synchronic multiple units together.

### Who do I talk to? ###

Email hans.p.harmon@gmail.com for more information.

### Dependencies ###

#### rpi_ws281x ####
Main driver for ws281x devices
https://github.com/jgarff/rpi_ws281x

#### nanite ####
Block driven C++ code using RxCPP.
https://bitbucket.org/hansharmon/nanite/src/master/

